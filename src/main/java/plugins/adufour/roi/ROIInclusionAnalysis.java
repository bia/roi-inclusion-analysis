package plugins.adufour.roi;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.apache.poi.ss.usermodel.Workbook;

import icy.gui.dialog.MessageDialog;
import icy.math.MathUtil;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.type.point.Point3D;
import icy.type.point.Point5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;
import plugins.kernel.roi.roi2d.ROI2DPoint;
import plugins.kernel.roi.roi3d.ROI3DArea;

/**
 * This plug-in takes 2 sets of ROI, and calculates spatial inclusion and distances from one set
 * w.r.t. to the other
 * 
 * @author Alexandre Dufour
 */
public class ROIInclusionAnalysis extends EzPlug implements ROIBlock
{
    // EzGUI

    EzVarSequence seqOuterROI = new EzVarSequence("Take outer ROI from");
    EzVarSequence seqInnerROI = new EzVarSequence("Take inner ROI from");
    EzVarBoolean includeOverlap = new EzVarBoolean("Include overlapping ROI", true);
    EzVarSequence inputSequence = new EzVarSequence("Input sequence (for path export)");

    // Headless

    VarROIArray outerROI = new VarROIArray("Outer (enclosing) ROI");
    VarROIArray innerROI = new VarROIArray("Inner (enclosed) ROI");

    // Output

    VarWorkbook workbook = new VarWorkbook("Analysis", (Workbook) null);

    private String fullPathSequence = null;

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Outer ROI", outerROI);
        inputMap.add("Inner ROI", innerROI);
        inputMap.add("Include overlap", includeOverlap.getVariable());
        inputMap.add("Input sequence", inputSequence.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("workbook", workbook);
    }

    @Override
    public void clean()
    {
    }

    @Override
    protected void initialize()
    {
        addEzComponent(seqOuterROI);
        addEzComponent(seqInnerROI);
        addEzComponent(includeOverlap);
        getUI().setParametersIOVisible(false);
    }

    @Override
    public void execute()
    {
        if (!isHeadLess())
        {
            // EzGUI: take ROI from the specified sequences
            outerROI.setValue(seqOuterROI.getValue(true).getROIs().toArray(new ROI[0]));
            innerROI.setValue(seqInnerROI.getValue(true).getROIs().toArray(new ROI[0]));
        }

        if (inputSequence.getValue() != null)
            fullPathSequence = inputSequence.getValue().getFilename();

        Workbook wb = Workbooks.createEmptyWorkbook();

        // Create the sheets
        final IcySpreadSheet summarySheet = Workbooks.getSheet(wb, "Summary");
        final IcySpreadSheet detailSheet = Workbooks.getSheet(wb, "Details");

        // Write headers
        if (fullPathSequence != null) {
            detailSheet.setRow(0, "Full path", "Enclosing ROI", "T", "X", "Y", "Z", "Enclosed ROI", "X", "Y", "Z",
                    "Oriented dist. to edge", "Oriented dist. to edge (%)", "Absolute dist. to edge");
            summarySheet.setRow(0, "Full path", "Enclosing ROI", "T", "X", "Y", "Z", "nb. enclosed ROI", "Mean oriented dist. to edge",
                    "Mean oriented dist. to edge (%)", "Mean absolute dist. to edge");
        }
        else {
            detailSheet.setRow(0, "Enclosing ROI", "T", "X", "Y", "Z", "Enclosed ROI", "X", "Y", "Z",
                    "Oriented dist. to edge", "Oriented dist. to edge (%)", "Absolute dist. to edge");
            summarySheet.setRow(0, "Enclosing ROI", "T", "X", "Y", "Z", "nb. enclosed ROI", "Mean oriented dist. to edge",
                    "Mean oriented dist. to edge (%)", "Mean absolute dist. to edge");
        }

        Processor processor = new Processor(SystemUtil.getNumberOfCPUs());
        ArrayList<Future<Object[][]>> tasks = new ArrayList<>(outerROI.size());

        try
        {
            for (final ROI enclosing : outerROI)
            {
                tasks.add(processor.submit(new Callable<Object[][]>()
                {
                    @Override
                    public Object[][] call() throws InterruptedException
                    {
                        ArrayList<Object[]> results = new ArrayList<>();

                        Point5D cOut = ROIMassCenterDescriptorsPlugin.computeMassCenter(enclosing);
                        int outT = (int) MathUtil.round(cOut.getT(), 0);
                        double outX = MathUtil.round(cOut.getX(), 2);
                        double outY = MathUtil.round(cOut.getY(), 2);
                        double outZ = MathUtil.round(cOut.getZ(), 2);
                        String outName = enclosing.getName();

                        int nbInside = 0;
                        double avgOD2E = 0, avgOD2E_pct = 0, avgAD2E = 0;
                        for (ROI enclosed : innerROI)
                        {
                            double[] distances = inclusionAnalysis(enclosing, enclosed, true);

                            if (!Double.isNaN(distances[0]))
                            {
                                nbInside++;

                                Point5D cIn = ROIMassCenterDescriptorsPlugin.computeMassCenter(enclosed);
                                double inX = MathUtil.round(cIn.getX(), 2);
                                double inY = MathUtil.round(cIn.getY(), 2);
                                double inZ = MathUtil.round(cIn.getZ(), 2);
                                String inName = enclosed.getName();

                                double oD2E = distances[0];
                                double oD2E_pct = distances[1];
                                double absD2E = distances[2];

                                avgOD2E += oD2E;
                                avgOD2E_pct += oD2E_pct;
                                avgAD2E += absD2E;

                                oD2E = MathUtil.round(oD2E, 2);
                                oD2E_pct = MathUtil.round(oD2E_pct, 2);
                                absD2E = MathUtil.round(absD2E, 2);

                                if (fullPathSequence != null) {
                                    results.add(new Object[]{fullPathSequence, outName, outT, outX, outY, outZ, inName, inX, inY, inZ, oD2E,
                                            oD2E_pct, absD2E});
                                }
                                else {
                                    // detailSheet.setRow(row, outName, outT, outX, outY, outZ, inName, inX, inY, inZ, oD2E, oD2E_pct, absD2E);
                                    results.add(new Object[]{outName, outT, outX, outY, outZ, inName, inX, inY, inZ, oD2E,
                                            oD2E_pct, absD2E});
                                }
                            }
                        }

                        avgOD2E = MathUtil.round(avgOD2E / nbInside, 2);
                        avgOD2E_pct = MathUtil.round(avgOD2E_pct / nbInside, 2);
                        avgAD2E = MathUtil.round(avgAD2E / nbInside, 2);

                        if (fullPathSequence != null) {
                            results.add(new Object[]{fullPathSequence, outName, outT, outX, outY, outZ, nbInside, avgOD2E, avgOD2E_pct,
                                    avgAD2E});
                        }
                        else {
                            // summarySheet.setRow(summaryRow, outName, outT, outX, outY, outZ, nbInside, avgOD2E, avgOD2E_pct, avgAD2E);
                            results.add(new Object[]{outName, outT, outX, outY, outZ, nbInside, avgOD2E, avgOD2E_pct,
                                    avgAD2E});
                        }

                        return results.toArray(new Object[][] {});
                    }
                }));
            }

            int detailRow = 0, summaryRow = 0;

            for (Future<Object[][]> task : tasks)
            {
                Object[][] dataLines = task.get();

                // All lines but the last go in the "details" sheet
                for (int i = 0; i < dataLines.length - 1; i++)
                    detailSheet.setRow(++detailRow, dataLines[i]);

                // The last line goes to the "summary" sheet
                summarySheet.setRow(++summaryRow, dataLines[dataLines.length - 1]);

                if (getStatus() != null)
                    getStatus().setCompletion((double) summaryRow / outerROI.size());
            }
        }
        catch (InterruptedException e)
        {
            if (!isHeadLess())
                MessageDialog.showDialog("ROI inclusion analysis process interrupted !");
            System.err.print("ROI inclusion analysis process interrupted !");

            workbook.setValue(null);
            return;
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }
        finally
        {
            tasks.clear();
            processor.shutdownNow();
        }

        workbook.setValue(wb);

        if (!isHeadLess())
        {
            // EzGUI: display the workbook
            Workbooks.show(wb, "Inclusion analysis");
        }
    }

    /**
     * This method performs the inclusion analysis of a so-called "inner" ROI with respect to a
     * so-called "outer" ROI. It is first determined whether the specified "inner" ROI is contained
     * in (or overlaps with) the specified "outer" ROI. If so, three distance values are measured:
     * <ul>
     * <li>An "oriented" distance (in pixels) from the center of the "inner" ROI to the edge of the
     * "outer" ROI, calculated along a ray going from the center of the "outer" ROI through the
     * center of the "inner" ROI</li>
     * <li>The equivalent, relative "oriented" distance from center (0) to edge (1).</li>
     * <li>The "non-oriented" (closest) distance from the center of the "inner" ROI.</li>
     * </ul>
     * 
     * @param outerROI
     *        the outer (i.e. enclosing) ROI
     * @param innerROI
     *        the inner (i.e. enclosed) ROI
     * @param allowPartialOverlap
     *        set to <code>true</code> if partially overlapping ROI should be considered as
     *        "inside", or <code>false</code> if they should be discarded
     * @return A 3-element array containing the measured distances (or <code>NaN</code> if the
     *         "inner" ROI is not inside the "outer" ROI).
     * @throws InterruptedException
     */
    public static double[] inclusionAnalysis(ROI outerROI, ROI innerROI, boolean allowPartialOverlap)
            throws InterruptedException
    {
        if (outerROI instanceof ROI3D)
        {
            if (innerROI instanceof ROI2D && ((ROI2D) innerROI).getZ() != -1)
            {
                int z = ((ROI2D) innerROI).getZ();
                innerROI = new ROI3DArea(((ROI2D) innerROI).getBooleanMask(true), z, z);
            }

            if (innerROI instanceof ROI3D) {
                ((ROI3D) innerROI).setC(((ROI3D) outerROI).getC());
                ((ROI3D) innerROI).setT(((ROI3D) outerROI).getT());
            }
        }

        boolean enclosure = outerROI.contains(innerROI);

        if (outerROI instanceof ROI2D && innerROI instanceof ROI2D)
        {
            int outerC = ((ROI2D) outerROI).getC();
            int innerC = ((ROI2D) innerROI).getC();
            if (outerC != -1 && outerC != innerC)
            {
                // @Stephane: channel mismatch!
                ((ROI2D) outerROI).setC(innerC);
                enclosure = outerROI.contains(innerROI);
                ((ROI2D) outerROI).setC(outerC);
            }
        }
        else if (outerROI instanceof ROI3D && innerROI instanceof ROI3D)
        {
            int outerC = ((ROI3D) outerROI).getC();
            int innerC = ((ROI3D) innerROI).getC();
            if (outerC != -1 && outerC != innerC)
            {
                // @Stephane: channel mismatch!
                ((ROI3D) outerROI).setC(innerC);
                enclosure = outerROI.contains(innerROI);
                ((ROI3D) outerROI).setC(outerC);
            }
        }

        if (!enclosure && allowPartialOverlap)
        {
            enclosure = outerROI.intersects(innerROI);
        }

        if (!enclosure)
            return new double[] {Double.NaN, Double.NaN, Double.NaN};

        Point5D p5;

        // get the center of the enclosing ROI
        p5 = ROIMassCenterDescriptorsPlugin.computeMassCenter(outerROI);
        Point3d outCenter = new Point3d(p5.getX(), p5.getY(), p5.getZ());

        // get the center of the enclosed ROI
        Point3d inCenter;
        if (innerROI instanceof ROI2DPoint)
        {
            Point2D p2 = ((ROI2DPoint) innerROI).getPoint();
            inCenter = new Point3d(p2.getX(), p2.getY(), 0.0);
        }
        else
        {
            p5 = ROIMassCenterDescriptorsPlugin.computeMassCenter(innerROI);
            inCenter = new Point3d(p5.getX(), p5.getY(), p5.getZ());
        }
        // compute the "oriented" inclusion distance (i.e. center to center)
        double distToCenter = outCenter.distance(inCenter);
        double absoluteDistToEdge = Double.MAX_VALUE;

        // compute (locally) the radius of the enclosing ROI
        // do this lazily by stepping away from the center
        double step = 0.1;
        Point3d p = new Point3d(inCenter);
        Vector3d ray = new Vector3d();
        ray.sub(inCenter, outCenter);
        ray.normalize();
        ray.scale(step);

        // step away until we exit the ROI
        // NB: we might already be outside if the enclosed ROI is only overlapping
        if (outerROI instanceof ROI2D)
        {
            // incrementally compute the oriented distance
            while (((ROI2D) outerROI).contains(p.x, p.y))
                p.add(ray);

            // Compute the raw distance to edge
            for (Point edgePt : ((ROI2D) outerROI).getBooleanMask(true).getContourPoints())
            {
                double rawDist = edgePt.distance(inCenter.x, inCenter.y);
                if (rawDist < absoluteDistToEdge)
                    absoluteDistToEdge = rawDist;
            }
        }
        else if (outerROI instanceof ROI3D)
        {
            // incrementally compute the oriented distance
            while (((ROI3D) outerROI).contains(p.x, p.y, p.z))
                p.add(ray);

            // Compute the raw distance to edge
            for (Point3D.Integer edgePt : ((ROI3D) outerROI).getBooleanMask(true).getContourPoints())
            {
                double rawDist = inCenter.distance(new Point3d(edgePt.x, edgePt.y, edgePt.z));
                if (rawDist < absoluteDistToEdge)
                    absoluteDistToEdge = rawDist;
            }
        }
        else
        {
            // incrementally compute the oriented distance
            while (outerROI.contains(p.x, p.y, p.z, -1, -1))
                p.add(ray);
        }

        // the distance from p to the center is the "local" radius
        double localRadius = p.distance(outCenter);
        double orientedDistToEdge = localRadius - distToCenter;

        return new double[] {orientedDistToEdge, distToCenter / localRadius, absoluteDistToEdge};
    }
}
